class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :payee
      t.date :date
      t.decimal :amount
      t.boolean :cleared
      t.date :date_cleared

      t.timestamps
    end
  end
end
