class Transaction < ActiveRecord::Base
  attr_accessible :amount, :cleared, :date, :date_cleared, :payee
  
  belongs_to :account
end
