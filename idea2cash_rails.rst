Web
=================


Installing Rails
------------------

For Windows and Mac, I would install Rails Installer from http://www.railsinstaller.org

Creating the Project
----------------------

.. code-block:: bash
  
  $ rails new dragon_hoard
  
This creates a new rails project.  

.. image:: images/app_folders.png


.. code-block:: bash
  
  $ rails server
  

Create Home
-------------

.. code-block:: bash

  $ rails genernate controller home index


Create Accounts 
------------------

.. code-block:: bash

  $ rails generate scaffold account name:string starting_balance:decimal 
  

.. code-block:: bash

  $ rake db:migrate

.. code-block:: bash
  
  $ rails server



Create Users
---------------

.. code-block:: ruby

  gem "devise", "~> 2.2.3"


.. code-block:: bash

  $ bundle install
  

.. code-block:: bash

  $ rails g devise:install
  
.. edit routes set root

.. edit config for development

.. edit layout to add notification

.. code-block:: bash

  $ rails g devise users
  
.. code-block:: bash

  $ rake db:migrate


.. add before_filter to application controller

.. add skip_before_filter to home controller

Create transactions 
----------------------

.. code-block:: bash

  $ rails generate scaffold transaction payee:string date:date amount:decimal memo:string cleared:boolean date_cleared:date account_id:integer  
  

.. code-block:: bash

  $ rake db:migrate

.. code-block:: bash
  
  $ rails server

.. define relationship between account and transaction

.. edit routes file to nest transaction to accounts

.. modify transaction controller to use the account parameter

.. create before filter to lookup account


